# Coconut
Coconut is a framework which aims to help the developer to easily make a java application
with a custom class loader. The class loader is supposed to allow dynamic dependency
loading and also fetch information and classes of the loaded dependencies.

# Importing
You can use Coconut from both maven or gradle, by simply importing the repository and
adding the project to your dependencies. The library itself is lightweight if you want
to create your own Coconut implementation, otherwise it's highly recommended to use the
existing implementation `CoconutTree`.

```xml
<repositories>
    <repository>
        <id>karmadev_snapshots</id>
        <url>https://nexus.karmadev.es/repository/snapshots/</url>
    </repository>
    <repository>
        <id>karmadev_releases</id>
        <url>https://nexus.karmadev.es/repository/internal/</url>
    </repository>
</respositories>
```

```groovy
repositories {
    mavenCentral()
    maven {
        url "https://nexus.karmadev.es/repository/snapshots/"
        name "karmadev_snapshots"
    }
    maven {
        url "https://nexus.karmadev.es/repository/internal/"
        name "karmadev_releases"
    }
}
```

Please note not both of the repositories are needed. If you are planing to use snapshot
versions which include untested and not-completely finished features you should use
snapshots (highly unrecommended for non-development environments).

```xml
<dependencies>
    <dependency>
        <groupId>es.karmadev.coconut</groupId>
        <artifactId>Coconut</artifactId>
        <version>{coconut version}</version>
        <scope>compile</scope>
    </dependency>
</dependencies>
```

```groovy
ext {
    coconutVersion = '<coconut version>'
}

dependencies {
    compile group: 'es.karmadev.coconut', name: 'Coconut', 'version': coconutVersion
}
```

I also recommend shading Coconut, so it doesn't break if another project in the same
environment is also using a different version of Coconut.

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.2.4</version>
        <executions>
            <execution>
                <phase>package</phase>
                <goals>
                    <goal>shade</goal>
                </goals>
                <configuration>
                    <createDependencyReducedPom>false</createDependencyReducedPom>

                    <relocations>
                        <relocation>
                            <pattern>es.karmadev.coconut</pattern>
                            <shadedPattern>com.domain.myproject.coconut</shadedPattern>
                        </relocation>
                    </relocations>
                </configuration>
            </execution>
        </executions>
    </plugin>
</plugins>
```

```groovy
plugins {
    id 'com.github.johnrengelman.shadow'
}

shadowJar {
    mergeServiceFiles()
    
    relocate 'es.karmadev.coconut', 'com.domain.project.coconut'
}
```
* I'm not a gradle user, gradle examples might be wrong

# [License](https://license.karmadev.es/)

# Credits
There are yet no contributors. When a PR gets accepted, the issuer of the PR will be
added here.