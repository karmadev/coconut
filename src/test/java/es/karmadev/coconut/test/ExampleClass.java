package es.karmadev.coconut.test;

public class ExampleClass {

    private final String name;
    private final int age;

    public ExampleClass(final String name, final int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }
}
