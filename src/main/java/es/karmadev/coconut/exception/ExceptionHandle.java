package es.karmadev.coconut.exception;

/**
 * Represents an exception handle
 */
public interface ExceptionHandle {

    /**
     * Get if the exception has been
     * handled
     *
     * @return if the exception is handled
     */
    boolean isHandled();

    /**
     * Mark the exception has handled
     */
    void handle();
}
