package es.karmadev.coconut.exception;

import java.io.PrintWriter;

/**
 * Represents an unhandled coconut exception
 */
public class RuntimeCoconutException extends RuntimeException implements ExceptionHandle {

    private boolean isHandled = true;

    /**
     * Initialize the coconut exception
     */
    public RuntimeCoconutException() {
        super();
    }

    /**
     * Initialize the coconut exception
     *
     * @param message the exception message
     */
    public RuntimeCoconutException(final String message) {
        super(message);
    }

    /**
     * Initialize the coconut exception
     *
     * @param cause the error cause
     */
    public RuntimeCoconutException(final Throwable cause) {
        super(cause);
    }

    /**
     * Initialize the coconut exception
     *
     * @param message the exception message
     * @param cause the error cause
     */
    public RuntimeCoconutException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Initialize the coconut exception
     *
     * @param message the exception message
     * @param cause the error cause
     * @param suppress if the suppression is enabled or not
     * @param writeStackTrace if the stack trace is write-able
     */
    public RuntimeCoconutException(final String message, final Throwable cause, final boolean suppress, final boolean writeStackTrace) {
        super(message, cause, suppress, writeStackTrace);
    }

    /**
     * Prints this throwable and its backtrace to the specified
     * print writer.
     *
     * @param s {@code PrintWriter} to use for output
     * @since JDK1.1
     */
    @Override
    public void printStackTrace(final PrintWriter s) {
        super.printStackTrace(s);
        handle();
    }

    /**
     * Returns the cause of this throwable or {@code null} if the
     * cause is nonexistent or unknown.  (The cause is the throwable that
     * caused this throwable to get thrown.)
     *
     * <p>This implementation returns the cause that was supplied via one of
     * the constructors requiring a {@code Throwable}, or that was set after
     * creation with the {@link #initCause(Throwable)} method.  While it is
     * typically unnecessary to override this method, a subclass can override
     * it to return a cause set by some other means.  This is appropriate for
     * a "legacy chained throwable" that predates the addition of chained
     * exceptions to {@code Throwable}.  Note that it is <i>not</i>
     * necessary to override any of the {@code PrintStackTrace} methods,
     * all of which invoke the {@code getCause} method to determine the
     * cause of a throwable.
     *
     * @return the cause of this throwable or {@code null} if the
     * cause is nonexistent or unknown.
     * @since 1.4
     */
    @Override
    public synchronized Throwable getCause() {
        handle();
        return super.getCause();
    }

    /**
     * Fills in the execution stack trace. This method records within this
     * {@code Throwable} object information about the current state of
     * the stack frames for the current thread.
     *
     * <p>If the stack trace of this {@code Throwable} {@linkplain
     * Throwable#Throwable(String, Throwable, boolean, boolean) is not
     * writable}, calling this method has no effect.
     *
     * @return a reference to this {@code Throwable} instance.
     * @see Throwable#printStackTrace()
     */
    @Override
    public synchronized Throwable fillInStackTrace() {
        handle();
        return super.fillInStackTrace();
    }

    /**
     * Provides programmatic access to the stack trace information printed by
     * {@link #printStackTrace()}.  Returns an array of stack trace elements,
     * each representing one stack frame.  The zeroth element of the array
     * (assuming the array's length is non-zero) represents the top of the
     * stack, which is the last method invocation in the sequence.  Typically,
     * this is the point at which this throwable was created and thrown.
     * The last element of the array (assuming the array's length is non-zero)
     * represents the bottom of the stack, which is the first method invocation
     * in the sequence.
     *
     * <p>Some virtual machines may, under some circumstances, omit one
     * or more stack frames from the stack trace.  In the extreme case,
     * a virtual machine that has no stack trace information concerning
     * this throwable is permitted to return a zero-length array from this
     * method.  Generally speaking, the array returned by this method will
     * contain one element for every frame that would be printed by
     * {@code printStackTrace}.  Writes to the returned array do not
     * affect future calls to this method.
     *
     * @return an array of stack trace elements representing the stack trace
     * pertaining to this throwable.
     * @since 1.4
     */
    @Override
    public StackTraceElement[] getStackTrace() {
        handle();
        return super.getStackTrace();
    }

    /**
     * Get if the exception has been
     * handled
     *
     * @return if the exception is handled
     */
    @Override
    public boolean isHandled() {
        return this.isHandled;
    }

    /**
     * Mark the exception has handled
     */
    @Override
    public void handle() {
        this.isHandled = true;
    }
}
