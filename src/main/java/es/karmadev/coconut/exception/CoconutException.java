package es.karmadev.coconut.exception;

/**
 * Generic Coconut exception
 */
public class CoconutException extends Exception implements ExceptionHandle {

    protected boolean isHandled;

    /**
     * Initialize the coconut exception
     */
    public CoconutException() {
        this(true);
    }

    /**
     * Initialize the coconut exception
     *
     * @param message the exception message
     */
    public CoconutException(final String message) {
        this(true, message);
    }

    /**
     * Initialize the coconut exception
     *
     * @param cause the error cause
     */
    public CoconutException(final Throwable cause) {
        this(true, cause);
    }

    /**
     * Initialize the coconut exception
     *
     * @param message the exception message
     * @param cause the error cause
     */
    public CoconutException(final String message, final Throwable cause) {
        this(true, message, cause);
    }

    /**
     * Initialize the coconut exception
     *
     * @param message the exception message
     * @param cause the error cause
     * @param suppress if the suppression is enabled or not
     * @param writeStackTrace if the stack trace is write-able
     */
    public CoconutException(final String message, final Throwable cause, final boolean suppress, final boolean writeStackTrace) {
        this(true, message, cause, suppress, writeStackTrace);
    }

    /**
     * Initialize the coconut exception
     *
     * @param handled if the error has been already handled
     */
    CoconutException(final boolean handled) {
        super();
        this.isHandled = handled;
    }

    /**
     * Initialize the coconut exception
     *
     * @param handled if the error has been already handled
     * @param message the exception message
     */
    CoconutException(final boolean handled, final String message) {
        super(message);
        this.isHandled = handled;
    }

    /**
     * Initialize the coconut exception
     *
     * @param handled if the error has been already handled
     * @param cause the error cause
     */
    CoconutException(final boolean handled, final Throwable cause) {
        super(cause);
        this.isHandled = handled;
    }

    /**
     * Initialize the coconut exception
     *
     * @param handled if the error has been already handled
     * @param message the exception message
     * @param cause the error cause
     */
    CoconutException(final boolean handled, final String message, final Throwable cause) {
        super(message, cause);
        this.isHandled = handled;
    }

    /**
     * Initialize the coconut exception
     *
     * @param handled if the error has been already handled
     * @param message the exception message
     * @param cause the error cause
     * @param suppress if the suppression is enabled or not
     * @param writeStackTrace if the stack trace is write-able
     */
    CoconutException(final boolean handled, final String message, final Throwable cause, final boolean suppress, final boolean writeStackTrace) {
        super(message, cause, suppress, writeStackTrace);
        this.isHandled = handled;
    }

    /**
     * Get if the exception has been
     * handled
     *
     * @return if the exception is handled
     */
    @Override
    public final boolean isHandled() {
        return this.isHandled;
    }

    /**
     * Mark the exception has handled
     */
    @Override
    public final void handle() {
        this.isHandled = true;
    }
}
