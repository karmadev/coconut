package es.karmadev.coconut.utils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public final class URISetMap {

    private final Map<URI, Set<String>> uriSetMap = new HashMap<>();

    public Map<URI, Set<String>> getUriSetMap() {
        return Collections.unmodifiableMap(uriSetMap);
    }

    /**
     * Tries to add the URL
     *
     * @param url the url
     * @return if the URL was successfully added
     * @throws URISyntaxException if the url URI syntax is not valid
     */
    public boolean add(final URL url) throws URISyntaxException {
        URI uri = url.toURI();

        Set<String> scanned = scan(uri);
        if (scanned.isEmpty()) return false;

        for (URI uriElement : uriSetMap.keySet()) {
            Set<String> setElement = uriSetMap.get(uriElement);
            if (setElement.equals(scanned)) return false;
        }

        uriSetMap.put(uri, scanned);
        return true;
    }

    /**
     * Scan a URI
     *
     * @param uri the URI
     * @return the scanned URI
     */
    private Set<String> scan(final URI uri) {
        Path path = Paths.get(uri);
        if (!Files.exists(path)) return Collections.emptySet();

        Set<String> foundRoutes = new HashSet<>();
        try (ZipFile zip = new ZipFile(path.toFile())) {
            Enumeration<? extends ZipEntry> entries = zip.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.isDirectory()) continue;

                String name = entry.getName().replace("/", ".");
                if (!name.endsWith(".class")) continue;

                name = name.substring(0, name.length() - 6);
                foundRoutes.add(name);
            }
        } catch (IOException ex) {
            return Collections.emptySet();
        }

        return foundRoutes;
    }
}
