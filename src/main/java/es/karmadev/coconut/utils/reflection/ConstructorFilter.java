package es.karmadev.coconut.utils.reflection;

import java.lang.reflect.Constructor;
import java.util.function.BiFunction;

/**
 * Constructor filter
 */
@FunctionalInterface
public interface ConstructorFilter extends BiFunction<Class<?>, Constructor<?>, Boolean> {

    /**
     * Applies this function to the given arguments.
     *
     * @param aClass      the first function argument
     * @param constructor the second function argument
     * @return the function result
     */
    @Override
    Boolean apply(final Class<?> aClass, final Constructor<?> constructor);
}
