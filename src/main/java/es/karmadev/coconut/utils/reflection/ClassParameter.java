package es.karmadev.coconut.utils.reflection;

/**
 * Represents a class parameter
 */
public class ClassParameter<T> {

    private final Class<? extends T> clazz;
    private final T value;

    private ClassParameter(final Class<? extends T> clazz, final T value) {
        this.clazz = clazz;
        this.value = value;
    }

    /**
     * Get the type
     *
     * @return the type
     */
    public Class<? extends T> getType() {
        return this.clazz;
    }

    /**
     * Get the value
     *
     * @return the value
     */
    public T getValue() {
        return this.value;
    }

    /**
     * Create a new class parameter
     *
     * @param clazz the parameter class
     * @return the parameter
     * @param <T> the parameter type
     */
    public static <T> ClassParameter<T> nullOf(final Class<T> clazz) {
        return new ClassParameter<>(clazz, null);
    }

    /**
     * Create a new class parameter
     *
     * @param clazz the parameter class
     * @param value the parameter value
     * @return the parameter
     * @param <T> the parameter type
     */
    public static <T> ClassParameter<T> valueOf(final Class<T> clazz, final T value) {
        return new ClassParameter<>(clazz, value);
    }
}
