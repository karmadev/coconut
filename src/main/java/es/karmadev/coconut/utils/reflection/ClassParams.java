package es.karmadev.coconut.utils.reflection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Represents a collection of
 * class parameters
 */
public class ClassParams {

    private final List<ClassParameter<?>> parameters = new ArrayList<>();

    private ClassParams(final List<ClassParameter<?>> parameters) {
        this.parameters.addAll(parameters);
    }

    /**
     * Get the parameters
     *
     * @return the parameters
     */
    public Collection<ClassParameter<?>> getParameters() {
        return Collections.unmodifiableList(this.parameters);
    }

    /**
     * Get the parameter types
     *
     * @return the parameter types
     */
    public Class<?>[] getParameterTypes() {
        Class<?>[] classes = new Class<?>[this.parameters.size()];

        int index = 0;
        for (ClassParameter<?> parameter : this.parameters) {
            classes[index++]  = parameter.getType();
        }

        return classes;
    }

    /**
     * Get the parameter values
     *
     * @return the parameter values
     */
    public Object[] getParameterValues() {
        Object[] values = new Object[this.parameters.size()];

        int index = 0;
        for (ClassParameter<?> parameter : this.parameters) {
            values[index++]  = parameter.getValue();
        }

        return values;
    }

    /**
     * Map the class param to match the
     * provided class parameter array
     *
     * @param sorter the class params sorter
     * @return the class params
     */
    public ClassParams map(final Class<?>[] sorter) {
        List<ClassParameter<?>> newOrder = new ArrayList<>();

        List<Integer> ignoreIndexes = new ArrayList<>();
        for (Class<?> current : sorter) {
            for (int j = 0; j < parameters.size(); j++) {
                if (ignoreIndexes.contains(j)) continue;

                ClassParameter<?> parameter = parameters.get(j);
                if (parameter.getType().isAssignableFrom(current)) {
                    newOrder.add(parameter);
                    ignoreIndexes.add(j);
                    break;
                }
            }
        }

        return new ClassParams(newOrder);
    }

    /**
     * Get a class params builder
     *
     * @return the class params builder
     */
    public static ClassParams.Builder builder() {
        return new Builder();
    }

    /**
     * Class params builder
     */
    public static class Builder {

        private final List<ClassParameter<?>> parameters = new ArrayList<>();

        private Builder() {}

        /**
         * Add a parameter
         *
         * @param parameter the parameter
         * @return the class params builder
         * @param <T> the parameter type
         */
        public <T> Builder withParam(final ClassParameter<T> parameter) {
            this.parameters.add(parameter);
            return this;
        }

        /**
         * Build the class params
         *
         * @return the class params
         */
        public ClassParams build() {
            return new ClassParams(parameters);
        }
    }
}
