package es.karmadev.coconut.utils;

import es.karmadev.coconut.exception.RuntimeCoconutException;
import es.karmadev.coconut.utils.reflection.ClassParams;
import es.karmadev.coconut.utils.reflection.ConstructorFilter;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents a class collection
 */
@SuppressWarnings("unused")
public class ClassCollection implements Iterable<Class<?>> {

    private final Class<?>[] classes;
    private ConstructorFilter constructorFilter = (aClass, constructor) -> true;

    /**
     * Initialize the class collection
     *
     * @param classes the classes contained in
     *                the collection
     */
    public ClassCollection(final Class<?>[] classes) {
        this.classes = classes;
    }

    /**
     * Set the constructor filter
     *
     * @param filter the filter
     */
    public void setConstructorFilter(final ConstructorFilter filter) {
        this.constructorFilter = filter;
    }

    /**
     * Get all the URLs in the collection
     *
     * @return the URLs
     */
    public Collection<Class<?>> getURLs() {
        return Arrays.asList(classes);
    }

    /**
     * Get if the class collections has
     * the specified class
     *
     * @param name the class name
     * @return if the class collection has
     * the provided class
     */
    public boolean hasClass(final String name) {
        return getClass(name, null) != null;
    }

    /**
     * Get if the class collections has
     * the specified class
     *
     * @param name the class name
     * @param filter the class filter
     * @return if the class collection has
     * the provided class
     */
    public boolean hasClass(final String name, final Function<Class<?>, Boolean> filter) {
        return getClass(name, filter) != null;
    }

    /**
     * Get a class by its name
     *
     * @param name the class name
     * @return the class
     */
    public Class<?> getClass(final String name) {
        return getClass(name, null);
    }

    /**
     * Get a class by its name
     *
     * @param name the class name
     * @param filter the class filter
     * @return the class
     */
    public Class<?> getClass(final String name, final Function<Class<?>, Boolean> filter) {
        for (Class<?> clazz : classes) {
            if (clazz.getSimpleName().equals(name) || clazz.getName().equals(name) ||
                    clazz.getCanonicalName().equals(name)) {
                if (filter != null && !filter.apply(clazz)) continue;

                return clazz;
            }
        }

        return null;
    }

    /**
     * Instantiate a class
     *
     * @param name the class name
     * @param params the class params
     * @return the class instance
     * @param <T> the class type
     * @throws RuntimeCoconutException if there's a problem during
     * instantiation
     */
    public <T> T instantiateClass(final String name, final ClassParams params) throws RuntimeCoconutException {
        return instantiateClass(name, null, params, true);
    }

    /**
     * Instantiate a class
     *
     * @param name the class name
     * @param filter the class filter
     * @param params the class params
     * @return the class instance
     * @param <T> the class type
     * @throws RuntimeCoconutException if there's a problem during
     * instantiation
     */
    public <T> T instantiateClass(final String name, final Function<Class<?>, Boolean> filter, final ClassParams params) throws RuntimeCoconutException {
        return instantiateClass(name, filter, params, true);
    }

    /**
     * Instantiate a class
     *
     * @param name the class name
     * @param params the class params
     * @param resolve if the constructor should resolve
     *                unsorted parameters
     * @return the class instance
     * @param <T> the class type
     * @throws RuntimeCoconutException if there's a problem during
     * instantiation
     */
    public <T> T instantiateClass(final String name, final ClassParams params, final boolean resolve) {
        return instantiateClass(name, null, params, resolve);
    }

    /**
     * Instantiate a class
     *
     * @param name the class name
     * @param filter the class filter
     * @param params the class params
     * @param resolve if the constructor should resolve
     *                unsorted parameters
     * @return the class instance
     * @param <T> the class type
     * @throws RuntimeCoconutException if there's a problem during
     * instantiation
     */
    @SuppressWarnings("unchecked")
    public <T> T instantiateClass(final String name, final Function<Class<?>, Boolean> filter, final ClassParams params, final boolean resolve) {
        Class<?> clazz = getClass(name, filter);
        if (clazz == null) return null;

        Constructor<?> constructor = findConstructor(clazz, params, resolve);
        if (constructor == null) return null;

        ClassParams mapped = (resolve ? params.map(constructor.getParameterTypes()) : params);
        try {
            return (T) constructor.newInstance(mapped.getParameterValues());
        } catch (ReflectiveOperationException | ClassCastException ex) {
            throw new RuntimeCoconutException(ex);
        }
    }

    private <T> Constructor<? extends T> findConstructor(final Class<? extends T> clazz, final ClassParams params, final boolean resolve) {
        try {
            Class<?>[] parameterTypes = params.getParameterTypes();

            try {
                Constructor<? extends T> constructor = clazz.getDeclaredConstructor(parameterTypes);
                if (constructorFilter == null || constructorFilter.apply(clazz, constructor)) return constructor;
            } catch (NoSuchMethodException ignored) {}

            try {
                Constructor<? extends T> constructor = clazz.getConstructor(parameterTypes);
                if (constructorFilter == null || constructorFilter.apply(clazz, constructor)) return constructor;
            } catch (NoSuchMethodException ignored) {}

            if (!resolve) return null;
            return findMatchConstructor(clazz, parameterTypes);
        } catch (SecurityException ex) {
            throw new RuntimeCoconutException(ex);
        }
    }

    private <T> Constructor<? extends T> findMatchConstructor(final Class<? extends T> clazz, final Class<?>[] parameterTypes) {
        Constructor<? extends T> constructor = getConstructor(clazz, clazz.getDeclaredConstructors(), parameterTypes);
        if (constructor == null) constructor = getConstructor(clazz, clazz.getConstructors(), parameterTypes);

        return constructor;
    }

    @SuppressWarnings("unchecked")
    private <T> Constructor<? extends T> getConstructor(final Class<? extends T> constructorClass, final Constructor<?>[] constructors, final Class<?>[] parameterTypes) {
        List<Class<?>> types = Arrays.asList(parameterTypes);

        Set<Constructor<?>> declaredConstructors = new HashSet<>(Arrays.asList((constructors)));
        List<Constructor<?>> declaredSorted = declaredConstructors.stream()
                .filter((constructor) -> constructor.getParameterCount() == types.size())
                .sorted(Comparator.comparingInt(Constructor::getParameterCount))
                .collect(Collectors.toList());
        Collections.reverse(declaredSorted);

        for (Constructor<?> constructor : declaredSorted) {
            if (constructorFilter != null && !constructorFilter.apply(constructorClass, constructor)) continue;

            List<Integer> checkedPositions = new ArrayList<>();
            for (Class<?> current : constructor.getParameterTypes()) {
                for (int i = 0; i < types.size(); i++) {
                    if (checkedPositions.contains(i)) continue;

                    Class<?> clazz = types.get(i);
                    if (clazz.isAssignableFrom(current)) {
                        checkedPositions.add(i);
                    }
                }
            }

            if (checkedPositions.size() == types.size()) return (Constructor<? extends T>) constructor;
        }

        return null;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @NotNull
    @Override
    public Iterator<Class<?>> iterator() {
        return Arrays.stream(classes).iterator();
    }

    /**
     * Performs the given action for each element of the {@code Iterable}
     * until all elements have been processed or the action throws an
     * exception.  Unless otherwise specified by the implementing class,
     * actions are performed in the order of iteration (if an iteration order
     * is specified).  Exceptions thrown by the action are relayed to the
     * caller.
     *
     * @param name the class name to filter for
     * @param action The action to be performed for each element
     * @throws NullPointerException if the specified action is null
     */
    public void forEach(final String name, final Consumer<? super Class<?>> action) {
        this.forEach(name, null, action);
    }

    /**
     * Performs the given action for each element of the {@code Iterable}
     * until all elements have been processed or the action throws an
     * exception.  Unless otherwise specified by the implementing class,
     * actions are performed in the order of iteration (if an iteration order
     * is specified).  Exceptions thrown by the action are relayed to the
     * caller.
     *
     * @param name the class name to filter for
     * @param filter the class filter
     * @param action The action to be performed for each element
     * @throws NullPointerException if the specified action is null
     */
    public void forEach(final String name, final Function<Class<?>, Boolean> filter, final Consumer<? super Class<?>> action) {
        for (Class<?> clazz : classes) {
            if (clazz.getSimpleName().equals(name) || clazz.getName().equals(name) ||
                    clazz.getCanonicalName().equals(name)) {
                if (filter != null && !filter.apply(clazz)) continue;

                action.accept(clazz);
            }
        }
    }
}