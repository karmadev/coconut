package es.karmadev.coconut.loader;

import es.karmadev.coconut.exception.CoconutException;
import es.karmadev.coconut.exception.RuntimeCoconutException;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;

/**
 * Represents the coconut loader
 */
public abstract class CoconutLoader extends ClassLoader {

    protected final Path librariesLocation;

    /**
     * Initialize the coconut loader
     *
     * @param librariesLocation the initial libraries' location.
     */
    public CoconutLoader(final Path librariesLocation) {
        this(librariesLocation, null);
    }

    /**
     * Initialize the coconut loader
     *
     * @param librariesLocation the initial libraries' location.
     * @param parent the parent class loader
     */
    public CoconutLoader(final Path librariesLocation, final ClassLoader parent) {
        super((parent != null ? parent : ClassLoader.getSystemClassLoader()));
        this.librariesLocation = librariesLocation;
    }

    /**
     * Get the loader libraries location
     *
     * @return the libraries location
     */
    public final Path getLibrariesLocation() {
        return this.librariesLocation;
    }

    /**
     * Set up the coconut loader
     *
     * @throws CoconutException if there's a problem during
     * setup
     */
    public abstract void setupLoader() throws CoconutException;

    /**
     * Get the coconut class loader
     *
     * @return a coconut class loader
     * @throws RuntimeCoconutException if the loader is null
     */
    @NotNull
    public abstract CoconutClassLoader getLoader() throws RuntimeCoconutException;
}
