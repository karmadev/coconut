package es.karmadev.coconut.loader;

import es.karmadev.coconut.exception.CoconutException;
import es.karmadev.coconut.exception.RuntimeCoconutException;
import es.karmadev.coconut.utils.ClassCollection;
import es.karmadev.coconut.utils.URISetMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.Set;
import java.util.zip.CRC32;

/**
 * Represents a Coconut class loader
 */
@SuppressWarnings("unused")
public abstract class CoconutClassLoader extends URLClassLoader {

    private final URISetMap set = new URISetMap();

    static {
        ClassLoader.registerAsParallelCapable();
    }

    /**
     * Constructs a new URLClassLoader for the given URLs. The URLs will be
     * searched in the order specified for classes and resources after first
     * searching in the specified parent class loader. Any URL that ends with
     * a '/' is assumed to refer to a directory. Otherwise, the URL is assumed
     * to refer to a JAR file which will be downloaded and opened as needed.
     *
     * <p>If there is a security manager, this method first
     * calls the security manager's {@code checkCreateClassLoader} method
     * to ensure creation of a class loader is allowed.
     *
     * @param resource              the internal resource path
     * @throws SecurityException    if a security manager exists and its
     *                              {@code checkCreateClassLoader} method doesn't allow
     *                              creation of a class loader.
     * @throws NullPointerException if {@code urls} is {@code null}.
     * @throws CoconutException if the class loader fails to get created
     * @see SecurityManager#checkCreateClassLoader
     */
    public CoconutClassLoader(final String resource) throws CoconutException {
        super(
                new URL[]{
                        extractURL(Thread.currentThread().getContextClassLoader(), resource)
                },
                Thread.currentThread().getContextClassLoader()
        );
    }

    /**
     * Constructs a new URLClassLoader for the given URLs. The URLs will be
     * searched in the order specified for classes and resources after first
     * searching in the specified parent class loader. Any URL that ends with
     * a '/' is assumed to refer to a directory. Otherwise, the URL is assumed
     * to refer to a JAR file which will be downloaded and opened as needed.
     *
     * <p>If there is a security manager, this method first
     * calls the security manager's {@code checkCreateClassLoader} method
     * to ensure creation of a class loader is allowed.
     *
     * @param resource              the internal resource path
     * @throws SecurityException    if a security manager exists and its
     *                              {@code checkCreateClassLoader} method doesn't allow
     *                              creation of a class loader.
     * @throws NullPointerException if {@code urls} is {@code null}.
     * @throws CoconutException if the class loader fails to get created
     * @see SecurityManager#checkCreateClassLoader
     */
    public CoconutClassLoader(final Path resource) throws CoconutException {
        super(
                new URL[]{
                        extractURL(Thread.currentThread().getContextClassLoader(), resource)
                },
                Thread.currentThread().getContextClassLoader()
        );
    }

    /**
     * Add the specified route to the
     * coconut class loader
     *
     * @param route the route to add
     * @return if the route was successfully added
     * @throws RuntimeCoconutException if the addition fails
     */
    public boolean add(final String route) throws RuntimeCoconutException {
        if (route == null) throw new RuntimeCoconutException("Invalid route");

        try {
            URI uri = URI.create(route);
            return add(uri);
        } catch (IllegalArgumentException ignored) {}

        try {
            URL url = new URL(route);
            return add(url);
        } catch (MalformedURLException ignored) {}

        try {
            File file = new File(route);
            if (file.exists()) return add(file);
        } catch (Throwable ignored) {}

        try {
            Path path = Paths.get(route);
            if (Files.exists(path)) return add(path);
            else throw new RuntimeCoconutException();
        } catch (Throwable ignored) {}

        throw new RuntimeCoconutException();
    }

    /**
     * Add the specified file to the
     * coconut class loader
     *
     * @param file the file to add
     * @return if the file was successfully added
     * @throws RuntimeCoconutException if the addition fails
     */
    public boolean add(final Path file) throws RuntimeCoconutException {
        return add(file.toUri());
    }

    /**
     * Add the specified file to the
     * coconut class loader
     *
     * @param file the file to add
     * @return if the file was successfully added
     * @throws RuntimeCoconutException if the addition fails
     */
    public boolean add(final File file) throws RuntimeCoconutException {
        return add(file.toURI());
    }

    /**
     * Add the specified URI to the
     * coconut class loader
     *
     * @param uri the uri to add
     * @return if the uri was successfully added
     * @throws RuntimeCoconutException if the addition fails
     */
    public boolean add(final URI uri) throws RuntimeCoconutException {
        try {
            return add(uri.toURL());
        } catch (MalformedURLException ex) {
            throw new RuntimeCoconutException(ex);
        }
    }

    /**
     * Add the specified URL to the
     * coconut class loader
     *
     * @param url the url to add
     * @return if the url was successfully added
     * @throws RuntimeCoconutException if the addition fails
     */
    public boolean add(final URL url) throws RuntimeCoconutException {
        try {
            if (set.add(url)) {
                super.addURL(url);
                return true;
            }
        } catch (Throwable ex) {
            throw new RuntimeCoconutException(ex);
        }

        return false;
    }

    /**
     * Get all the URLs that has been
     * loaded
     *
     * @return the loaded URLs
     * @throws RuntimeCoconutException if there's a problem during
     * collection
     */
    public URL[] getLoadedURLs() throws RuntimeCoconutException {
        Map<URI, Set<String>> uriSetMap = set.getUriSetMap();
        URL[] array = new URL[uriSetMap.size()];

        int index = 0;
        for (URI uri : uriSetMap.keySet()) {
            try {
                URL url = uri.toURL();
                array[index++] = url;
            } catch (MalformedURLException ex) {
                throw new RuntimeCoconutException(ex);
            }
        }

        return array;
    }

    /**
     * Get all the classes that has been
     * loaded
     *
     * @return the loaded classes
     * @throws RuntimeCoconutException if there's a problem during
     * collection
     */
    public Class<?>[] getLoadedClasses() throws RuntimeCoconutException {
        Map<URI, Set<String>> uriSetMap = set.getUriSetMap();
        int count = uriSetMap.values().stream()
                .mapToInt(Set::size).sum();

        Class<?>[] array = new Class<?>[count];

        int index = 0;
        for (Set<String> scanned : uriSetMap.values()) {
            for (String rawClass : scanned) {
                try {
                    Class<?> clazz = Class.forName(rawClass);
                    array[index++] = clazz;
                } catch (ClassNotFoundException | NoClassDefFoundError ex) {
                    throw new RuntimeCoconutException(ex);
                }
            }
        }

        return array;
    }

    /**
     * Get all the classes from the registered
     * route
     *
     * @param route the route to fetch classes
     *              for
     * @return the class collection
     * @throws RuntimeCoconutException if the collection fails
     */
    public ClassCollection getClasses(final String route) throws RuntimeCoconutException {
        if (route == null) throw new RuntimeCoconutException("Invalid route");

        try {
            URI uri = URI.create(route);
            return getClasses(uri);
        } catch (IllegalArgumentException ignored) {}

        try {
            URL url = new URL(route);
            return getClasses(url);
        } catch (MalformedURLException ignored) {}

        try {
            File file = new File(route);
            if (file.exists()) return getClasses(file);
        } catch (Throwable ignored) {}

        try {
            Path path = Paths.get(route);
            if (Files.exists(path)) return getClasses(path);
            else throw new RuntimeCoconutException();
        } catch (Throwable ignored) {}

        throw new RuntimeCoconutException();
    }

    /**
     * Get all the classes from the registered
     * file
     *
     * @param file the file to fetch classes
     *              for
     * @return the class collection
     * @throws RuntimeCoconutException if the collection fails
     */
    public ClassCollection getClasses(final Path file) throws RuntimeCoconutException {
        return getClasses(file.toUri());
    }

    /**
     * Get all the classes from the registered
     * file
     *
     * @param file the file to fetch classes
     *              for
     * @return the class collection
     * @throws RuntimeCoconutException if the collection fails
     */
    public ClassCollection getClasses(final File file) throws RuntimeCoconutException {
        return getClasses(file.toURI());
    }

    /**
     * Get all the classes from the registered
     * url
     *
     * @param url the url to fetch classes
     *              for
     * @return the class collection
     * @throws RuntimeCoconutException if the collection fails
     */
    public ClassCollection getClasses(final URL url) throws RuntimeCoconutException {
        try {
            return getClasses(url.toURI());
        } catch (URISyntaxException ex) {
            throw new RuntimeCoconutException(ex);
        }
    }

    /**
     * Get all the classes from the registered
     * uri
     *
     * @param uri the uri to fetch classes
     *              for
     * @return the class collection
     * @throws RuntimeCoconutException if the collection fails
     */
    public ClassCollection getClasses(final URI uri) throws RuntimeCoconutException {
        Map<URI, Set<String>> uriSetMap = set.getUriSetMap();
        if (!uriSetMap.containsKey(uri) || uriSetMap.get(uri) == null)
            throw new RuntimeCoconutException("No loaded element " + uri + " in the coconut class loader");

        Set<String> scanned = uriSetMap.get(uri);

        Class<?>[] elements = new Class<?>[scanned.size()];
        int index = 0;
        for (String rawClass : scanned) {
            try {
                Class<?> clazz = Class.forName(rawClass);
                elements[index++] = clazz;
            } catch (ClassNotFoundException | NoClassDefFoundError ex) {
                throw new RuntimeCoconutException(ex);
            }
        }

        return new ClassCollection(elements);
    }

    /**
     * Extract a resource
     *
     * @param parentClassLoader the parent class loader of the class loader
     * @param resourcePath      the resource to extract
     * @return the extracted URL
     * @throws CoconutException if there's a problem during extraction
     */
    private static URL extractURL(final ClassLoader parentClassLoader, final String resourcePath) throws CoconutException {
        if (!(parentClassLoader instanceof CoconutLoader))
            throw new CoconutException("CoconutClassLoader needs to be loaded by CoconutLoader");

        CoconutLoader coconutLoader = (CoconutLoader) parentClassLoader;
        URL resource = parentClassLoader.getResource(resourcePath);
        if (resource == null) {
            throw new CoconutException("Failed to locate " + resourcePath);
        }

        return extractURL(parentClassLoader, resource, resourcePath, true);
    }

    /**
     * Extract a resource
     *
     * @param parentClassLoader the parent class loader of the class loader
     * @param resourcePath      the resource to extract
     * @return the extracted URL
     * @throws CoconutException if there's a problem during extraction
     */
    private static URL extractURL(final ClassLoader parentClassLoader, final Path resourcePath) throws CoconutException {
        if (!(parentClassLoader instanceof CoconutLoader))
            throw new CoconutException("CoconutClassLoader needs to be loaded by CoconutLoader");

        if (!Files.exists(resourcePath) || Files.isDirectory(resourcePath))
            throw new CoconutException("Cannot extract " + resourcePath + " because it's a directory or does not exist");

        CoconutLoader coconutLoader = (CoconutLoader) parentClassLoader;
        try {
            URL resource = resourcePath.toUri().toURL();
            return extractURL(parentClassLoader, resource, null, false);
        } catch (MalformedURLException ex) {
            throw new CoconutException(ex);
        }
    }

    /**
     * Extract a resource
     *
     * @param parentClassLoader the parent class loader of the class loader
     * @param resource          the resource to extract
     * @param resourcePath      the resource name
     * @param extract           if the resource should be extracted
     * @return the extracted URL
     * @throws CoconutException if there's a problem during extraction
     */
    private static URL extractURL(final ClassLoader parentClassLoader, final URL resource, final String resourcePath, final boolean extract) throws CoconutException {
        if (!extract) return resource;

        if (!(parentClassLoader instanceof CoconutLoader))
            throw new CoconutException("CoconutClassLoader needs to be loaded by CoconutLoader");

        CoconutLoader coconutLoader = (CoconutLoader) parentClassLoader;
        Path path;
        try {
            String name = getName(resourcePath);
            path = coconutLoader.getLibrariesLocation().resolve(name);
            if (Files.exists(path)) {
                compareAndExtract(resource, path);
            } else {
                extract(resource, path);
            }

            return path.toUri().toURL();
        } catch (IOException ex) {
            throw new CoconutException(ex);
        }
    }

    private static String getName(final String fileName) {
        String rawName = fileName;
        if (fileName.contains("/")) {
            String[] data = fileName.split("/");
            int lastIndex = (fileName.endsWith("/") ? data.length - 2 : data.length - 1);

            rawName = data[lastIndex];
        }

        if (rawName.contains(".")) {
            String[] nameSplitter = rawName.split("\\.");
            String extension = nameSplitter[nameSplitter.length - 1];

            return rawName.substring(0, rawName.length() - (extension.length() + 1));
        }

        return String.format("%s.packedjar", rawName);
    }

    private static void compareAndExtract(final URL resource, final Path file) throws IOException {
        boolean extract = false;
        long resourceValue = getCrc(resource.openStream());
        long fileValue = getCrc(file.toUri().toURL().openStream());

        if (resourceValue != fileValue) {
            extract(resource, file);
        }
    }

    private static void extract(final URL resource, final Path path) throws IOException {
        try (InputStream stream = resource.openStream()) {
            Files.copy(stream, path, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    private static long getCrc(final InputStream stream) throws IOException {
        try {
            CRC32 crc = new CRC32();

            int read;
            byte[] byteBuff = new byte[4096];
            while ((read = stream.read(byteBuff)) != -1) {
                crc.update(byteBuff, 0, read);
            }

            return crc.getValue();
        } finally {
            stream.close();
        }
    }
}
