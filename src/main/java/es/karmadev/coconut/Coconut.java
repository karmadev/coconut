package es.karmadev.coconut;

import es.karmadev.coconut.exception.CoconutException;
import es.karmadev.coconut.exception.RuntimeCoconutException;
import org.jetbrains.annotations.NotNull;

/**
 * Coconut element, the core of the
 * core
 */
public abstract class Coconut {

    private static Coconut instance;

    /**
     * Register the current coconut instance
     * as the desired coconut
     *
     * @throws CoconutException if the coconut instance fails
     * to entirely initiate
     */
    protected void registerInstance() throws CoconutException {
        if (instance != null && instance.equals(this)) return;
        if (instance != null) {
            throw new CoconutException("Another instance of coconut is already running in the same JVM");
        }

        instance = this;
        /*try {
            initialize();
        } catch (Throwable ex) {
            if (!(ex instanceof CoconutException)) {
                throw new UnhandledCoconutException(ex);
            }

            throw ex;
        }*/
    }

    /**
     * Get the Coconut name
     *
     * @return the coconut instance name
     */
    @NotNull
    public abstract String getName();

    /**
     * Initialize the instance
     *
     * @throws CoconutException if there's a problem
     * during initialization
     */
    public abstract void initialize() throws CoconutException;

    /**
     * Get the coconut instance
     *
     * @return the coconut instance
     * @throws RuntimeCoconutException if the coconut instance has not
     * been yet defined
     */
    @NotNull
    public static Coconut getInstance() throws RuntimeCoconutException {
        if (instance == null) throw new RuntimeCoconutException("Coconut instance has not been defined!");
        return instance;
    }
}
