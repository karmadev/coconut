# Contributing
The project is licensed under MIT, meaning you can copy and alter the code as you want
without needing to contribute, but I would appreciate a lot any contribution.

You can contribute by forking Coconut and making a pull request. Note that making a pull
request won't instantly make your changes official, but will be reviewed and rejected or
accepted. If rejected, a comment on the PR will be added explaining why.